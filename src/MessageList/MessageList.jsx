import React from "react";
import { Message } from "../Message/Message";
import { OwnMessage } from "../OwnMessage/OwnMessage";
import List from '@material-ui/core/List';

class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.userId = props.userId;
    }


    messageDivider(date) {
        let dividerMessage;
        const today = new Date();
        var days = new Date(today - date).getDate() - 1;
        if (days < 1) {
            dividerMessage = "Today";
        } else if (days < 2) {
            dividerMessage = "Yesterday";
        } else {
            const day = date.getDate();
            const longMonth = date.toLocaleString('en-us', { month: 'long' });
            const weekday = new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(date);
            dividerMessage = weekday + ", " + day + " " + longMonth;
        }
        return (
            <div className="messages-divider"
            >{dividerMessage}
            </div>
        );
    }

    render() {
        return (
            <List className="message-list">
                {this.props.messages.map((message, index) => {
                    if (message.userId === this.userId) {
                        return <>
                            <OwnMessage
                                key={index}
                                editMessage={this.props.editMessage}
                                deleteMessage={this.props.deleteMessage}
                                message={message}
                                {...message}
                            />;
                        </>
                    }
                    return <Message key={index} {...message} likeMessage={this.props.likeMessage} />

                })}
            </List>
        )
    }
}

export { MessageList }