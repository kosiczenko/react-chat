import React from 'react'
import Button from '@material-ui/core/Button';

class MessageInput extends React.Component {
    constructor() {
        super()
        this.state = {
            message: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        this.setState({
            message: e.target.value,
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.sendMessage(this.state.message);
        this.setState({
            message: ''
        });
        var textEdit = document.getElementsByClassName("message-input-text")[0];
        textEdit.value = '';
    }

    render() {
        return (
            <form className="send-message-form">
                <div className="message-input">
                    <input className="message-input-text" placeholder="Message" onChange={this.handleChange}>
                    </input>

                    <Button className="message-input-button"
                        color="primary"
                        onClick={this.handleSubmit} >Send</Button>
                    <input id="editMessageId" type="hidden"/>
                </div >
            </form >
        )
    }

}

export { MessageInput }