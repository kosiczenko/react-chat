import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chat from './Chat';

ReactDOM.render(
  <React.StrictMode>
    <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" 
    userAvatar = "https://resizing.flixster.com/ebfx_F45QE9nqBehja2jWOP3YX0=/300x300/v1.cjs0OTc5O2o7MTc2ODU7MTIwMDs0ODY7NjUy"
    userdId="11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
    userName="Me"
    />
  </React.StrictMode>,
  document.getElementById('root')
);


