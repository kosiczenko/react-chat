import React from "react";

class Header extends React.Component {
    render() {
        const { participants, messagesCount, lastMessageTime } = this.props;

        var dateFormat = require('dateformat');
        var messageDate = dateFormat(Date.parse(lastMessageTime), "dd.mm.yyyy HH:MM", true);

        return (
            <div className="header">
                <div className="header-title">My chat</div>
                <div className="header-users-count">
                    {participants}
                </div>
                <div className="header-messages-count">
                    {messagesCount}
                </div>
                <div className="header-last-message-date">
                    {messageDate}
                </div>
            </div>
        )
    }
}

export { Header }