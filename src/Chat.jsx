import React from 'react';
import { MessageList } from './MessageList/MessageList';
import { MessageInput } from './MessageInput/MessageInput';
import { Preloader } from './Preloader/Preloader'
import { Header } from './Header/Header'
import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import uuid from 'react-uuid'

class Chat extends React.Component {
    constructor() {
        super();
        this.state = {
            messages: [],
            loading: true,
            participants: 0,
            lastMessageTime: 0,

        }
        this.sendMessage = this.sendMessage.bind(this)
    }

    componentDidMount() {
        fetch(this.props.url)
            .then(response => response.json())
            .then(data => this.setState({
                messages: data,
                loading: false,
                lastMessageTime: data[data.length - 1].createdAt,
                participants: new Set(data.map(message => message.userId)).size,
            }))
            .catch((err) => {
                console.log(err);
                this.setState({ loading: false });
            });
    }


    sendMessage(text) {
        if (text === undefined || text === "") {
            return;
        }

        var id = document.getElementById("editMessageId").value;
        if (id !== undefined && id !== "") {
            const messageList = this.state.messages.map(message =>
            (message.id !== id ?
                message :
                {
                    ...message,
                    text: text,
                    editedAt: new Date()
                }
            )
            );
            this.setState({ messages: messageList });

            return;
        }

        const messageTime = Date();
        const newMessage = {
            "id": uuid(),
            "avatar": this.props.userAvatar,
            "text": text,
            "createdAt": messageTime,
            "user": this.props.userName,
            "userId": this.props.userId,
        };

        var newMessages = [...this.state.messages, newMessage];

        this.setState({
            messages: newMessages,
            lastMessageTime: messageTime,
            participants: new Set(newMessages.map(message => message.userId)).size
        });

    }

    editMessage = (inMessage) => {
        document.getElementById("editMessageId").value = inMessage.id;
        document.getElementsByClassName("message-input-text")[0].value = inMessage.text;
    }

    deleteMessage = (id) => {
        const messageList = this.state.messages.filter(m => m.id !== id);
        this.setState({ messages: messageList });
    }

    likeMessage = (id) => {
        const messageList = this.state.messages.map(message =>
            message.id !== id ? message :
                {
                    ...message,
                    liked: !message.liked
                })

        this.setState({ messages: messageList });
    }

    render() {
        const { messages, loading } = this.state;

        return (
            <div className="chat">
                <Paper elevation={3}>
                    <Container maxWidth="md">
                        <Header participants={this.state.participants} messagesCount={this.state.messages.length} lastMessageTime={this.state.lastMessageTime} />
                        {loading ?
                            (<Preloader />) :
                            <MessageList messages={messages}
                                deleteMessage={this.deleteMessage}
                                editMessage={this.editMessage}
                                likeMessage={this.likeMessage} />
                        }
                        <MessageInput
                            sendMessage={this.sendMessage} />
                    </Container>
                </Paper>
            </div>
        );
    }

}

export default Chat